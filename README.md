# [coolgrandads.com](coolgrandads.com)
Hosted on a multi-cloud k3s cluster

# [Wordpress](https://wordpress.org/)
A content management / blog platform.
- github: https://github.com/WordPress/WordPress
- docker: https://hub.docker.com/_/wordpress/
- mysql backend (password saved as blackbox secret)

# Secrets
- Secrets encrypted using [BlackBox](https://github.com/StackExchange/blackbox) 
- Admin password set at runtime

# Deployment
Deployment is on a Kubernetes cluster running under `fritzdavenport.com`, using helm and helmfile
```
helmfile diff
```
```
helmfile apply
```